//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./registrosv2.json');

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.get("/", function (req, res) {
  //res.send("Hola Mundo NodeJS!!!");
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/Clientes", function (req, res) {
  res.send("Aqui estan los clientes...");
  //res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/Clientes/:idCliente", function (req, res) {
  res.send("Aqui tiene al cliente numero: "+req.params.idCliente);
  //res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/Movimientos/v1", function (req, res) {
  //res.send("Aqui tiene al cliente numero: "+req.params.idCliente);
  res.sendfile("registros.json");
});

app.get("/Movimientos/v2/:index", function (req, res) {
  //res.send("Aqui tiene al cliente numero: "+req.params.idCliente);
  console.log(req.params.index);
  res.send(movimientosJSON[req.params.index]);
  //res.sendfile("registrosv2.json");
});

app.get("/Movimientos/v2", function (req, res) {
  //res.send("Aqui tiene al cliente numero: "+req.params.idCliente);
  // console.log(req.query);
  // res.send("recibido");
  res.send(movimientosJSON);
  //res.sendfile("registrosv2.json");
});

app.post("/", function (req, res) {
  res.send("Hemos recibido su peticion post V2");
});


app.post("/Movimientos/v2", function (req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento Dado de Alta");
});
